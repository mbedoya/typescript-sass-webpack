export class AppService {

    getWelcomeMessage(): string {
        return "Hello Everybody!";
    }

    getNewMessage(): string {
        return "Yes Yes Yes";
    }
}

export class AdvancedAppService extends AppService{

    getCurrentHour(): number{
        return new Date().getHours();
    }

    getTimeMessage(): string{
        const currentHour = this.getCurrentHour();

        if (currentHour < 12) {
            return "It is " + currentHour + " the morning, enjoy your day";
        } else {
            return "It is " + currentHour + " hours already";
        }
    }

}