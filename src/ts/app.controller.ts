import { AdvancedAppService } from './app.service';

export class AppController {

    private appService;
    private mainContainer;

    constructor() {
        this.appService = new AdvancedAppService();
        window.addEventListener("load", this.onInit.bind(this));
    }

    onInit(): void{
        this.mainContainer = document.getElementById("main-container");
        const newMessageButton = document.getElementById("new-message-button");
        const timeButton = document.getElementById("time-button");
        
        this.mainContainer.innerHTML = this.appService.getWelcomeMessage();
        newMessageButton.addEventListener("click", this.onGetNewMessage.bind(this));
        timeButton.addEventListener("click", this.onGetTimeMessage.bind(this));
    }

    onGetNewMessage(): void {
        this.mainContainer.innerHTML = this.appService.getNewMessage();
    }

    onGetTimeMessage(): void {
        this.mainContainer.innerHTML = this.appService.getTimeMessage();
    }
}